# mybatis数据权限插件

#### 介绍

#### 软件架构
#### 软件架构说明
#### spring-boot-starter mybatis

#### 主要解决的问题是 , 不用显示的去对每个Dao接口注入判断权限的对象 , 例如User。 只需要在Mapper接口上添加@AuthData ， 就可以在mapper.xml文件中通过指定表达式获取User的参数。

#### 注意事项:此插件不预防SQL注入,所以应避免直接使用外部传入值。（而事实上数据权限一般也不依赖信赖外部信息）

#### 使用方法

#### 最最最下方有示例代码
#### 1.下载项目,打成jar包,或者直接下载jar包
#### 2.在spring-boot中引入
#### 3.实现 IAuthData 接口并注册到spring容器中, 其中getAuthType()为分组名, getDataAuth()为解析权限控制语句时,获取权限数据的方法
#### 4.在 mybatis.Dao的接口方法上,加上@AuthData,可以以数组方式传入数据分组名称,如果不传则调用所有IAuthData实例的getDataAuth()方法获取数据
#### 5.在对应的sql标签里,加入权限控制语句:
##### 例如:
    getDataAuth() 返回: 
        { "userType": 1 , "orgList": [ "001" , "002" ] , "areaCode" : 1001 , "bol": true }
    
    在sql中写下: 
        #( userType == 1 )[ AND org_id IN @{ orgList } ]
        
    最终会解析为: 
        AND org_id IN ("001","002")
    
    #[ AND status = @{ bol } ] 解释为 AND status = 1
    
#### 规则
##### 标签以#开头,后面衔接"()"或者"[]"
##### "()"类似if标签,内容为ognl表达式,最终结果应为boolean类型;可以省略,则默认有效
##### "[]"包含任意内容,可以包含@{}取值标签
##### @{} ognl表达式,结果支持基本类型,String,Collection集合;boolean类型会转为1和0,集合类型会转为(value1,value2....)
#### ognl表达式,就是mybatis,if-test里用的



#### 大致执行过程
##### 1.mybatis Dao接口执行
##### 2.触发mybatis拦截器"com.xuanyue.mybatisplug.plug.DataAuthPlug.intercept()"
##### 3.拦截器中执行DataAuthHandler.parseSql()进行判断解析
##### 3.判断该接口是否集成@AuthData , 如果没有继承就不解析sql直接返回原sql
##### 4.如果继承自@AuthData , 从Spring上下文中获取注解中指定的IAuthData实现类 , 执行getDataAuth() , 获取权限判断对象
##### 5.用获取到的权限数据解析sql , 返回解析后的sql语句
##### 6.回到mybatis的正常执行流程

##### 代码示例：

```

    /**
     * 实现 IAuthData
     * 首先实现用与获取权限对象的接口
     */
    @Component
    public class AuthDataDemo implements IAuthData{

        /* 返回权限类型名称 , 用于指定权限数据范围 */
        @Override
        public String getAuthType() {
            return "demoAuthData";
        }

        /* 返回切实的数据 */
        @Override
        public Map<String, Object> getDataAuth() {

            Map<String,Object> result = new HashMap<>();
            result.put( "name" , "甲" );
            result.put( "age" , 32 );

            List<String> hobbys = new LinkedList<>();
            hobbys.add( "看码云" );
            hobbys.add( "看马云" );

            result.put( "hobbys" , hobbys );
            result.put( "isBoy" , true );
            result.put( "invalid" , false );
            
            return result;
        }
    }

    /**
     * Mapper接口
     */
    @Mapper
    public interface DemoMapper{
        
        /**
         * 注意 , 这里没有传入参数
         * 也可以不指定 @AuthData 的参数
         * 那么会调用所有注册在Spring容器中的IAuthData.getDataAuth()
         * 如果只有一个实现类 , 不写更方便
         */
        @AuthData( { "demoAuthData" } )
        public String DemoQuery();
        
    }

    <!-- 这里是DemoMapper.xml文件 -->
    <select id="DemoQuery" resultType="String">
        
        SELECT 
            "返回值" 
        FROM 
            dual
        WHERE
            1 = 1

        #( name != null and name.length() > 0 )
        [ AND "乙" = @{ name } ]
        
        <!-- 此处没有圆括号"() ",默认判断成功 -->
        #[ AND 32 = @{ age } ]

        <!-- 集合类型自动展开,保留手写IN是为了视觉可读性 -->
        #( hobbys != null and hobbys.size() > 0 )
        [ AND "看码云" IN @{ hobbys } ]
        
        <!-- 布尔类型会自动转为0和1 -->
        #( isBoy )
        [ AND 1 = @{ isBoy } ]
        
        <!-- invalid 的值是fasle , 这句应该不会渲染 -->
        #( invalid )
        [ AND 1 = 2 ]
    
    </select>
===========================================================
最终SQL:
        SELECT 
            "返回值" 
        FROM 
            dual
        WHERE
            1 = 1
        AND "乙" = "甲"
        AND 32 = 32
        AND "看码云" IN ( "看码云" , "看马云" )
        AND 1 = 1
```